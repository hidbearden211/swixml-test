package swixml.forms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swixml.SwingEngine;
import swixml.service.EvaluateExpressionService;

import javax.swing.*;
import javax.swing.plaf.synth.SynthLookAndFeel;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

public class CalculatorComponent extends JPanel {
    private final static Logger log = LogManager.getLogger(CalculatorComponent.class.getName());

    private SwingEngine sw = new SwingEngine(this);

    //Components
    public JTextField expression;
    public JLabel rezultEval;

    //Actions
    public Action eval = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            log.debug("#eval");
            rezultEval.setText(EvaluateExpressionService.getInstance().eval(expression.getText()));
        }
    };

    public CalculatorComponent() {
        log.debug("loading component");
        try {
            sw.insert("ui_xml/calculator-component.xml", this);
        } catch (Exception e) {
            log.error("loading component error");
            e.printStackTrace();
        }

        //Style
        new Style(this);
    }
}

