package swixml.forms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swixml.SwingEngine;
import swixml.service.NoteWriterService;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class NotesComponents extends JPanel {
    private Logger log = LogManager.getLogger(NotesComponents.class.getName());

    private SwingEngine sw = new SwingEngine(this);

    //Components
    public JTextField header;
    public JTextArea note;

    //Actions
    public Action addNote = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            log.debug("#addNode");
            NoteWriterService.getInstance().addNote(header.getText(),note.getText());
            header.setText("New title");
            note.setText("Your message");
        }
    };

    public NotesComponents(){
        log.debug("loading component");
        try {
            sw.insert("ui_xml/note-component.xml", this);
            //new Style(this);
        } catch (Exception e) {
            log.error("loading component error");
            e.printStackTrace();
        }
    }
}
