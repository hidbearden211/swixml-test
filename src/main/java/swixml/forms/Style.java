package swixml.forms;

import javax.swing.*;
import javax.swing.plaf.synth.SynthLookAndFeel;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;

public class Style {
    //style

    public Style(Container c) {
        SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
        try {
            lookAndFeel.load(new FileInputStream(new File("D:\\workspace\\java\\InterviewTestTask\\swixml-one-proj\\src\\main\\resources\\ui_xml\\style\\some_style.xml")),
                             c.getClass());
//            lookAndFeel.load(new URL("file:///D:\\workspace\\java\\InterviewTestTask\\swixml-one-proj\\src\\main\\resources\\ui_xml\\style\\some_style.xml"));
            UIManager.setLookAndFeel(lookAndFeel);
            //
        } catch (UnsupportedLookAndFeelException | ParseException | IOException e) {
            System.err.println("Couldn't get specified look and feel ("
                                       + lookAndFeel
                                       + "), for some reason.");
            System.err.println("Using the default look and feel.");
            e.printStackTrace();
        }
    }
}
