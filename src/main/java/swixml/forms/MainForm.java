package swixml.forms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swixml.SwingEngine;

import java.awt.*;

public class MainForm {

    private final static Logger log = LogManager.getLogger(MainForm.class.getName());
    public MainForm() {
        log.debug("Load main frame");
        try {
            SwingEngine sw = new SwingEngine(this);
            sw.getTaglib().registerTag("calc-component", CalculatorComponent.class);
            sw.getTaglib().registerTag("note-component", NotesComponents.class);
            Container c = sw.render("ui_xml/main-frame.xml");
            c.setVisible(true);
            //new Style(c);
        } catch (Exception e) {
            log.error("Loading mainframe error");
            e.printStackTrace();
        }
    }
}
