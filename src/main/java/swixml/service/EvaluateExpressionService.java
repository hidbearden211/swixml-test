package swixml.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class EvaluateExpressionService {
    private final static Logger log = LogManager.getLogger(EvaluateExpressionService.class.getName());

    private static EvaluateExpressionService ourInstance = new EvaluateExpressionService();

    public static EvaluateExpressionService getInstance() {
        return ourInstance;
    }

    private ScriptEngine engine;

    private EvaluateExpressionService() {
        ScriptEngineManager factory = new ScriptEngineManager();
        engine = factory.getEngineByName("nashorn");
    }

    public String eval(String expression) {
        log.debug("#eval evaluate expression= {}", expression);
        try {
            return engine.eval("eval(" + expression + ")").toString();
        } catch (final ScriptException se) {
            se.printStackTrace();
        }
        return "We tried to do something";
    }
}

