package swixml.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class NoteWriterService {
    private final Logger log = LogManager.getLogger(NoteWriterService.class.getName());

    private static NoteWriterService ourInstance = new NoteWriterService();

    public static NoteWriterService getInstance() {
        return ourInstance;
    }

    private NoteWriterService() {
    }

    public void addNote(String header, String message) {
        log.debug("#addNote header= {} ; message= {}", header, message);
        try (PrintWriter out = new PrintWriter(new File("out.txt"))) {
            out.println(header);
            out.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
